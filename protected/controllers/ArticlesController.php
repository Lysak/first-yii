<?php

class ArticlesController extends Controller
{
	public function actionIndex()
	{
		// $this->render('index');

		// $model = new Articles;

		// $model->title = 'Lysak 1';
		// $model->author = 'Dmytrii Lysak';
		// $model->save(false);

		// $a = Articles::model()->findAll();
		
		// $this->render('index', array('model' => $a));

		$criteria = new CDbCriteria();
		// $criteria->select = "date";
		// $criteria->order = "date desc";
		$criteria->order='t.id DESC';
		// $criteria->order='t.id DESC';

		$count=Articles::model()->count($criteria);
	
		$pages=new CPagination($count);
		// элементов на страницу
		$pages->pageSize=5;
		$pages->applyLimit($criteria);
	
		$model = Articles::model()->findAll($criteria);
	
		$this->render('index', array(
			'model' => $model,
			'pages' => $pages,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}